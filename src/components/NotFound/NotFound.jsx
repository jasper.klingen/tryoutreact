import {Link} from 'react-router-dom'

function NotFound() {
    return(
        <main>
            <h4>Page does not exist</h4>
            <Link to="/">take me home, country roads</Link>
        </main>
    )
}
export default NotFound