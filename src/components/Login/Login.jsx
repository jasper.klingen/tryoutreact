import { useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AppContainer from '../../hoc/AppContainer'
import { loginAttemptAction } from '../../Store/actions/loginActions'


const Login = () => {

    const dispatch = useDispatch()
    const  { loginError, loginAttempting} = useSelector(state => state.loginReducer)

    const [ credentials, setCredentials] = useState({
        username: '',
        password: ''
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        }) 
    }
    const onFormSubmit = event => {
        event.preventDefault() // stop the page reloading
        dispatch(loginAttemptAction(credentials))
    }

     return (
        <AppContainer>
            <form className="mt-3 mb-3" onSubmit={ onFormSubmit}>
                <h1>Login to Sweder forum</h1>

                <p>welkom 😊</p>
                <div className="mb-3">
                    <label htmlFor="" className="form-label">Username</label>
                    <input onChange={onInputChange} 
                    id="username" type="text" placeholder="Enter your username" className="form-control" />
                </div>
                <div className="mb-3">
                    <label htmlFor="" className="form-label">Password</label>
                    <input onChange={onInputChange} 
                    id="password" type="password" placeholder="Enter your password" className="form-control" />
                </div>
                <button type="submit" className="btn btn-primary btn-lg">Login</button>

                
            </form>
            { loginAttempting &&
                <p>trying to login...</p>
            }
            { loginError &&
                    <div className="alert alert-danger" role="alert">
                        <h4>Unsuccesful</h4>
                        <p className="mb 0">{loginError} </p>
                    </div>
                }
        </AppContainer>
    )
}
export default Login