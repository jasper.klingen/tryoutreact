import { applyMiddleware } from 'redux'
import {loginMiddleWare} from './loginMiddelware'
import { sessionMiddleware } from './sessionMiddleware'

export default applyMiddleware(
    loginMiddleWare,
    sessionMiddleware
)