export const ACTION_SESSION_SET = '[session] SET'

export const sessionSetAction = profiel => ({
    type: ACTION_SESSION_SET,
    payload: profiel
})