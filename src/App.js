import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Login from './components/Login/Login';
import About from './components/About/About';
import NotFound from './components/NotFound/NotFound';
import AppContainer from './hoc/AppContainer'



function App() {
  return (
    <BrowserRouter>

      <div className="App">
        <AppContainer>
          <h1>React Txt forum</h1>
          <h4>
            <span className="material-icons">
              accessibility
        </span>
          </h4>
        </AppContainer>



        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/*" component={NotFound} />
          <Route path="/about" component={About} />
        </Switch>
      </div>
    </BrowserRouter>

  );
}

export default App;
